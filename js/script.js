var typed = new Typed('#typed', {
    stringsElement: '#typed-strings',
    backSpeed: 50,
    typeSpeed: 50,
    loop: true,
    suffle: true,
    showCursor: false
});


var color_arr = ['#20BF6B', '#027BFF', '#FC5C65'];

function color_change(j) {
    document.getElementById('typed').style.color = color_arr[j];
}

function loop_color() {
    setTimeout(function () {
        color_change(0);
        setTimeout(function () {
            color_change(1);
            setTimeout(function () {
                color_change(2);
                loop_color();
            }, 2000);
        }, 2000);
    }, 2000);
}
loop_color();
